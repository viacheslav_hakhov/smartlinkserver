#include <QCoreApplication>
#include <exception>
#include <iostream>
#include <memory>

#include "Log.hpp"
#include "Logger.hpp"
#include "source/QDatabase.hpp"
#include "source/QNetwork.hpp"
#include "source/config/ConfigWrapper.hpp"

Q_DECLARE_METATYPE( size_t )
Q_DECLARE_METATYPE( std::string )

void register_meta_types( )
{
    qRegisterMetaType< size_t >( "size_t" );
    qRegisterMetaType< std::string >( "std::string" );
}

int main( int argc, char* argv[] )
try
{
    QCoreApplication a( argc, argv );
    register_meta_types( );

    const ConfigWrapper config_wrapper( "config.json" );
    const ConfigData config_data = config_wrapper.get_config_data( );
    Logger::init( convert_config_level( config_data.log_level ) );

    LOG( LogType::INFO, MSG( "Server started!" ) );
    std::unique_ptr< INetwork > network( new QNetwork( config_data ) );

    return a.exec( );
}
catch ( const std::invalid_argument& ia )
{
    LOG( LogType::ERROR, MSG( ia.what( ) ) );
}

catch ( const std::exception& ex )
{
    LOG( LogType::ERROR, MSG( ex.what( ) ) );
}
