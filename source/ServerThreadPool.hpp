#ifndef SERVERTHREADPOOL_HPP
#define SERVERTHREADPOOL_HPP

#include <condition_variable>
#include <functional>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>

#include "interfaces/IServerHandler.hpp"

class ThreadPool
{
public:
    ThreadPool( std::size_t thread_count = std::thread::hardware_concurrency( ) );
    ~ThreadPool( );

    ThreadPool( const ThreadPool& ) = delete;
    ThreadPool( ThreadPool&& ) = delete;
    ThreadPool& operator=( const ThreadPool& ) = delete;
    ThreadPool& operator=( ThreadPool&& ) = delete;

    using work_item_t = std::function< void( void ) >;
    void run( work_item_t wi );

private:
    std::queue< std::unique_ptr< work_item_t > > m_queue;
    std::mutex m_queue_lock;
    std::condition_variable m_condition;
    std::vector< std::thread > m_threads;
};

#endif  // QUEUETHREADING_HPP
