#include "QDatabase.hpp"

#include <ctime>
#include <exception>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <sstream>

#include "Log.hpp"
#include "interfaces/IUserDataStorage.hpp"

namespace
{
    constexpr char const* DB_USERS_TABLE = "users";
    constexpr char const* DB_MESSAGES_TABLE = "messages";

    constexpr char const* DB_USERS_ID = "id";
    constexpr char const* DB_USERS_NICKNAME = "nickname";
    constexpr char const* DB_USERS_NAME = "name";
    constexpr char const* DB_USERS_SURNAME = "surname";
    constexpr char const* DB_USERS_PASSWORD = "password";
    constexpr char const* DB_USERS_PHONE_NUMBER = "phone_number";
    constexpr char const* DB_USERS_REGISTRATION_TIME = "registration_time";
    constexpr char const* DB_USERS_LAST_ACTIVITY = "last_activity";

    constexpr char const* DB_MESSAGES_ID = "id";
    constexpr char const* DB_MESSAGES_USER_NICKNAME = "user_nickname";
    constexpr char const* DB_MESSAGES_MESSAGE = "message";
    constexpr char const* DB_MESSAGES_TIME = "time";

    std::time_t subtract_time_in_days( std::time_t time1, int days )
    {
        struct tm* tm = localtime( &time1 );
        tm->tm_mday -= days;
        time_t previous_time = mktime( tm );
        return previous_time;
    }

    void generate_default_history_dates( int days, std::string& timestamp_from, std::string& timestamp_to )
    {
        std::time_t current_time = std::time( nullptr );
        std::time_t current_time2 = current_time;
        std::tm gm_time_to = *std::localtime( &current_time );

        std::time_t current_time_t2 = subtract_time_in_days( current_time2, days );
        std::tm gm_time_from = *std::localtime( &current_time_t2 );

        std::stringstream str_time;
        str_time << std::put_time( &gm_time_from, "%Y-%m-%d %T" );
        timestamp_from = str_time.str( );
        std::stringstream str_time2;
        str_time2 << std::put_time( &gm_time_to, "%Y-%m-%d %T" );
        timestamp_to = str_time2.str( );
    }

    std::string generate_time( )
    {
        std::time_t current_time = std::time( nullptr );
        std::tm gm_time = *std::localtime( &current_time );
        std::stringstream str_time;
        str_time << std::put_time( &gm_time, "%Y-%m-%d %T" );
        return str_time.str( );
    }

}  // namespace

enum USER
{
    USER_ID,
    NICKNAME,
    NAME,
    SURNAME,
    PASSWORD,
    PHONE_NUMBER,
    REGISTRATION_TIME,
    LAST_ACTIVITY
};

enum MESSAGE
{
    MESSAGE_ID,
    USER_NICKNAME,
    MESSAGE_DATA,
    TIME
};

bool do_file_exist( const char* fileName )
{
    std::ifstream infile( fileName );
    return infile.good( );
}

void db_error( bool check_query_success, const std::string& error )
{
    if ( !check_query_success )
    {
        throw std::logic_error( error );
    }
}

void db_tables_init( QSqlDatabase& chat_db )
{
    QSqlQuery query( chat_db );
    bool check_query_success = query.exec( QString( "CREATE TABLE '%1' ("
                                                    "'%2' integer PRIMARY KEY AUTOINCREMENT,"
                                                    "'%3'	text NOT NULL UNIQUE,"
                                                    "'%4'	text NOT NULL,"
                                                    "'%5'	text NOT NULL,"
                                                    "'%6'	text NOT NULL,"
                                                    "'%7'	text NOT NULL,"
                                                    "'%8'	datetime DEFAULT current_timestamp,"
                                                    "'%9'	datetime DEFAULT current_timestamp)" )
                                               .arg( DB_USERS_TABLE,
                                                     DB_USERS_ID,
                                                     DB_USERS_NICKNAME,
                                                     DB_USERS_NAME,
                                                     DB_USERS_SURNAME,
                                                     DB_USERS_PASSWORD,
                                                     DB_USERS_PHONE_NUMBER,
                                                     DB_USERS_REGISTRATION_TIME,
                                                     DB_USERS_LAST_ACTIVITY ) );
    db_error( check_query_success, query.lastError( ).text( ).toStdString( ) );

    check_query_success = query.exec( QString( "CREATE TABLE '%1' ("
                                               "'%2'	integer PRIMARY KEY AUTOINCREMENT,"
                                               "'%3'	text NOT NULL,"
                                               "'%4'	text NOT NULL,"
                                               "'%5'	datetime DEFAULT current_timestamp, "
                                               "FOREIGN KEY('%3') REFERENCES '%6' ('%3') )" )
                                          .arg( DB_MESSAGES_TABLE,
                                                DB_MESSAGES_ID,
                                                DB_MESSAGES_USER_NICKNAME,
                                                DB_MESSAGES_MESSAGE,
                                                DB_MESSAGES_TIME,
                                                DB_USERS_TABLE ) );
    db_error( check_query_success, query.lastError( ).text( ).toStdString( ) );
}

IUserDataStorage::ErrorType error_detection( QSqlError::ErrorType qsql_error_type )
{
    switch ( qsql_error_type )
    {
        case QSqlError::ErrorType::NoError:
            return IUserDataStorage::ErrorType::NO_ERROR;
        case QSqlError::ErrorType::ConnectionError:
            return IUserDataStorage::ErrorType::CONNECTION_ERROR;
        case QSqlError::ErrorType::StatementError:
            return IUserDataStorage::ErrorType::SYNTAX_ERROR;
        case QSqlError::ErrorType::TransactionError:
            return IUserDataStorage::ErrorType::RECORD_ERROR;
        case QSqlError::ErrorType::UnknownError:
            return IUserDataStorage::ErrorType::UNKNOWN_ERROR;
        default:
            return IUserDataStorage::ErrorType::NO_ERROR;
    }
}

QDatabase::QDatabase( const std::string& db_path, int history_time )
    : m_users_table( QSqlTableModel( nullptr, m_chat_db ) )
    , m_messages_table( QSqlTableModel( nullptr, m_chat_db ) )
    , m_history_time( history_time )
{
    m_chat_db = QSqlDatabase::addDatabase( "QSQLITE" );
    const bool do_db_file_exists = do_file_exist( db_path.c_str( ) );
    LOG( LogType::INFO, MSG( do_db_file_exists ? "File exists" : "File doesn`t exist" ) );
    m_chat_db.setDatabaseName( db_path.c_str( ) );

    if ( !m_chat_db.open( ) )
    {
        LOG( LogType::ERROR, MSG( "Chat.db Open Error" ) );
        throw std::logic_error( MSG( "Chat.db Open Error" ) );
    }
    LOG( LogType::INFO, MSG( "Chat.db opened" ) );

    if ( !do_db_file_exists )
    {
        try
        {
            db_tables_init( m_chat_db );
            LOG( LogType::INFO, MSG( "New tables in DB was created" ) );
        }
        catch ( const std::logic_error& e )
        {
            LOG( LogType::ERROR, MSG( "Create tables error!" ) );
            throw e;
        }
    }
    else
    {
        LOG( LogType::INFO, MSG( "Database file has been already created" ) );
    }

    m_users_table.setTable( ::DB_USERS_TABLE );
    m_messages_table.setTable( ::DB_MESSAGES_TABLE );
}

QDatabase::~QDatabase( )
{
    m_chat_db.close( );
}

IUserDataStorage::ErrorType QDatabase::get_user_data( const std::string& nickname, UserInfo& user_info )
{
    QSqlQuery query( m_chat_db );
    const bool success = query.exec(
        QString( "select * from %1 where %2 = '%3'" ).arg( DB_USERS_TABLE, DB_USERS_NICKNAME, nickname.c_str( ) ) );
    if ( !success )
    {
        LOG( LogType::ERROR,
             MSG( std::to_string( query.lastError( ).type( ) ) + " " + query.lastError( ).text( ).toStdString( ) ) );
        return error_detection( query.lastError( ).type( ) );
    }

    if ( query.next( ) )
    {
        if ( !query.isValid( ) )
        {
            LOG(
                LogType::ERROR,
                MSG( std::to_string( query.lastError( ).type( ) ) + " " + query.lastError( ).text( ).toStdString( ) ) );
            return error_detection( query.lastError( ).type( ) );
        }

        user_info = { query.value( USER::USER_ID ).toInt( ),
                      query.value( USER::NICKNAME ).toString( ).toStdString( ),
                      query.value( USER::NAME ).toString( ).toStdString( ),
                      query.value( USER::SURNAME ).toString( ).toStdString( ),
                      query.value( USER::PASSWORD ).toString( ).toStdString( ),
                      query.value( USER::PHONE_NUMBER ).toString( ).toStdString( ),
                      query.value( USER::REGISTRATION_TIME ).toDateTime( ).toString( ).toStdString( ),
                      query.value( USER::LAST_ACTIVITY ).toDateTime( ).toString( ).toStdString( ) };
    }

    return IUserDataStorage::ErrorType::NO_ERROR;
}

IUserDataStorage::ErrorType QDatabase::update_last_activity_time( const std::string& username,
                                                                  const std::string& new_last_activity_time )
{
    UserInfo user_info{ };
    get_user_data( username, user_info );

    if ( !user_info.is_empty( ) )
    {
        QSqlQuery query( m_chat_db );
        const bool success = query.exec( QString( "update %1 set %2 = '%3' where %4 = '%5' " )
                                             .arg( DB_USERS_TABLE,
                                                   DB_USERS_LAST_ACTIVITY,
                                                   new_last_activity_time.c_str( ),
                                                   DB_USERS_NICKNAME,
                                                   username.c_str( ) ) );
        if ( !success )
        {
            return error_detection( query.lastError( ).type( ) );
        }
        return IUserDataStorage::ErrorType::NO_ERROR;
    }

    return IUserDataStorage::ErrorType::RECORD_ERROR;
}

IUserDataStorage::ErrorType QDatabase::get_user_data( int id, UserInfo& user_info )
{
    QSqlQuery query( m_chat_db );
    const bool success = query.exec( QString( "select * from %1 where %2 = '%3'" )
                                         .arg( DB_USERS_TABLE, DB_USERS_ID, std::to_string( id ).c_str( ) ) );

    if ( !success )
    {
        return error_detection( query.lastError( ).type( ) );
    }
    if ( query.next( ) )
    {
        if ( !query.isValid( ) )
        {
            LOG(
                LogType::ERROR,
                MSG( std::to_string( query.lastError( ).type( ) ) + " " + query.lastError( ).text( ).toStdString( ) ) );

            return error_detection( query.lastError( ).type( ) );
        }

        user_info = { query.value( USER::USER_ID ).toInt( ),
                      query.value( USER::NICKNAME ).toString( ).toStdString( ),
                      query.value( USER::NAME ).toString( ).toStdString( ),
                      query.value( USER::SURNAME ).toString( ).toStdString( ),
                      query.value( USER::PASSWORD ).toString( ).toStdString( ),
                      query.value( USER::PHONE_NUMBER ).toString( ).toStdString( ),
                      query.value( USER::REGISTRATION_TIME ).toDateTime( ).toString( ).toStdString( ),
                      query.value( USER::LAST_ACTIVITY ).toDateTime( ).toString( ).toStdString( ) };
    }

    return IUserDataStorage::ErrorType::NO_ERROR;
}

IUserDataStorage::ErrorType QDatabase::get_last_activity( const std::string& nickname, std::string& last_activity )
{
    QSqlQuery query( m_chat_db );
    const bool success = query.exec(
        QString( "select * from %1 where %2 = '%3'" ).arg( DB_USERS_TABLE, DB_USERS_NICKNAME, nickname.c_str( ) ) );
    if ( !success )
    {
        return error_detection( query.lastError( ).type( ) );
    }

    if ( query.next( ) )
    {
        if ( !query.isValid( ) )
        {
            LOG(
                LogType::ERROR,
                MSG( std::to_string( query.lastError( ).type( ) ) + " " + query.lastError( ).text( ).toStdString( ) ) );

            return error_detection( query.lastError( ).type( ) );
        }

        last_activity = query.value( USER::LAST_ACTIVITY ).toDateTime( ).toString( ).toStdString( );
    }

    return IUserDataStorage::ErrorType::NO_ERROR;
}

IUserDataStorage::ErrorType QDatabase::get_all_last_activities( std::map< std::string, std::string >& users_activities )
{
    QSqlQuery query( m_chat_db );
    const bool success = query.exec(
        QString( "select %1, %2 from %3" ).arg( DB_USERS_NICKNAME, DB_USERS_LAST_ACTIVITY, DB_USERS_TABLE ) );
    if ( !success )
    {
        return error_detection( query.lastError( ).type( ) );
    }

    while ( query.next( ) )
    {
        if ( !query.isValid( ) )
        {
            LOG(
                LogType::ERROR,
                MSG( std::to_string( query.lastError( ).type( ) ) + " " + query.lastError( ).text( ).toStdString( ) ) );

            return error_detection( query.lastError( ).type( ) );
        }

        users_activities[ query.record( ).value( DB_USERS_NICKNAME ).toString( ).toStdString( ) ]
            = query.record( ).value( DB_USERS_LAST_ACTIVITY ).toString( ).toStdString( );
    }

    return IUserDataStorage::ErrorType::NO_ERROR;
}

IUserDataStorage::ErrorType QDatabase::add_user( const UserInfo& data )
{
    UserInfo user_exist{ };
    get_user_data( data.nickname, user_exist );
    if ( !user_exist.is_empty( ) )
    {
        return IUserDataStorage::ErrorType::RECORD_EXIST;
    }

    QSqlQuery query( m_chat_db );
    const bool success
        = query.exec( QString( "insert into %1 ( %2, %3, %4, %5, %6 ) values ( '%7', '%8', '%9', '%10', '%11' ) " )
                          .arg( DB_USERS_TABLE,
                                DB_USERS_NICKNAME,
                                DB_USERS_NAME,
                                DB_USERS_SURNAME,
                                DB_USERS_PASSWORD,
                                DB_USERS_PHONE_NUMBER,
                                data.nickname.c_str( ),
                                data.name.c_str( ),
                                data.surname.c_str( ),
                                data.password.c_str( ),
                                data.phone_number.c_str( ) ) );
    if ( !success )
    {
        return error_detection( query.lastError( ).type( ) );
    }
    return IUserDataStorage::ErrorType::NO_ERROR;
}

IUserDataStorage::ErrorType QDatabase::update_user( const UserInfo& data )
{
    UserInfo user_info;
    get_user_data( data.id, user_info );

    QSqlQuery query( m_chat_db );
    const bool success = query.exec( QString( "insert or replace into %1 ( %2, %3, %4, %5, %6, %7, %8 ) values ( '%9', "
                                              "'%10', '%11', '%12', '%13', '%14', '%15' ) " )
                                         .arg( DB_USERS_TABLE,
                                               DB_USERS_ID,
                                               DB_USERS_NICKNAME,
                                               DB_USERS_NAME,
                                               DB_USERS_SURNAME,
                                               DB_USERS_PASSWORD,
                                               DB_USERS_PHONE_NUMBER,
                                               DB_USERS_REGISTRATION_TIME,
                                               std::to_string( user_info.id ).c_str( ),
                                               data.nickname.c_str( ),
                                               data.name.c_str( ),
                                               data.surname.c_str( ),
                                               data.password.c_str( ),
                                               data.phone_number.c_str( ),
                                               data.registration_time.c_str( ) ) );
    if ( !success )
    {
        return error_detection( query.lastError( ).type( ) );
    }
    return IUserDataStorage::ErrorType::NO_ERROR;
}

IUserDataStorage::ErrorType QDatabase::get_messages( const std::string& datetime_from,
                                                     const std::string& datetime_to,
                                                     std::vector< UserMessage >& messages )
{
    QSqlQuery query( m_chat_db );
    const bool success
        = query.exec( QString( "select * from %1 where %2 between '%3' and '%4'" )
                          .arg( DB_MESSAGES_TABLE, DB_MESSAGES_TIME, datetime_from.c_str( ), datetime_to.c_str( ) ) );
    if ( !success )
    {
        return error_detection( query.lastError( ).type( ) );
    }

    while ( query.next( ) )
    {
        if ( !query.isValid( ) )
        {
            LOG(
                LogType::ERROR,
                MSG( std::to_string( query.lastError( ).type( ) ) + " " + query.lastError( ).text( ).toStdString( ) ) );

            return error_detection( query.lastError( ).type( ) );
        }

        messages.push_back( UserMessage( query.value( MESSAGE::USER_NICKNAME ).toString( ).toStdString( ),
                                         query.value( MESSAGE::MESSAGE_DATA ).toString( ).toStdString( ),
                                         query.value( MESSAGE::TIME ).toDateTime( ).toString( ).toStdString( ) ) );
    }
    return IUserDataStorage::ErrorType::NO_ERROR;
}

IUserDataStorage::ErrorType QDatabase::get_messages( std::vector< UserMessage >& messages )
{
    std::string datetime_from, datetime_to;
    ::generate_default_history_dates( m_history_time, datetime_from, datetime_to );

    QSqlQuery query( m_chat_db );
    const bool success
        = query.exec( QString( "select * from %1 where %2 between '%3' and '%4'" )
                          .arg( DB_MESSAGES_TABLE, DB_MESSAGES_TIME, datetime_from.c_str( ), datetime_to.c_str( ) ) );
    if ( !success )
    {
        return error_detection( query.lastError( ).type( ) );
    }

    while ( query.next( ) )
    {
        if ( !query.isValid( ) )
        {
            LOG(
                LogType::ERROR,
                MSG( std::to_string( query.lastError( ).type( ) ) + " " + query.lastError( ).text( ).toStdString( ) ) );

            return error_detection( query.lastError( ).type( ) );
        }

        messages.push_back( UserMessage( query.value( MESSAGE::USER_NICKNAME ).toString( ).toStdString( ),
                                         query.value( MESSAGE::MESSAGE_DATA ).toString( ).toStdString( ),
                                         query.value( MESSAGE::TIME ).toString( ).toStdString( ) ) );
    }
    return IUserDataStorage::ErrorType::NO_ERROR;
}

IUserDataStorage::ErrorType QDatabase::get_messages( std::vector< UserMessage >& messages,
                                                     const std::string& datetime_from )
{
    const std::string datetime_to = ::generate_time( );

    QSqlQuery query( m_chat_db );
    const bool success
        = query.exec( QString( "select * from %1 where %2 between '%3' and '%4'" )
                          .arg( DB_MESSAGES_TABLE, DB_MESSAGES_TIME, datetime_from.c_str( ), datetime_to.c_str( ) ) );
    if ( !success )
    {
        return error_detection( query.lastError( ).type( ) );
    }

    while ( query.next( ) )
    {
        if ( !query.isValid( ) )
        {
            LOG(
                LogType::ERROR,
                MSG( std::to_string( query.lastError( ).type( ) ) + " " + query.lastError( ).text( ).toStdString( ) ) );

            return error_detection( query.lastError( ).type( ) );
        }

        messages.push_back( UserMessage( query.value( MESSAGE::USER_NICKNAME ).toString( ).toStdString( ),
                                         query.value( MESSAGE::MESSAGE_DATA ).toString( ).toStdString( ),
                                         query.value( MESSAGE::TIME ).toString( ).toStdString( ) ) );
    }
    return IUserDataStorage::ErrorType::NO_ERROR;
}

IUserDataStorage::ErrorType QDatabase::get_messages( const std::string& day_data, std::vector< UserMessage >& messages )
{
    const std::string& datetime_from = day_data + " 00:00:00";
    const std::string& datetime_to = day_data + " 23:59:59";

    QSqlQuery query( m_chat_db );
    const bool success
        = query.exec( QString( "insert into %1 (%2, %3) values ('%4', '%5')" )
                          .arg( DB_MESSAGES_TABLE, DB_MESSAGES_USER_NICKNAME, DB_MESSAGES_MESSAGE, "maria", "hi" ) );
    if ( !success )
    {
        return error_detection( query.lastError( ).type( ) );
    }

    while ( query.next( ) )
    {
        if ( !query.isValid( ) )
        {
            LOG(
                LogType::ERROR,
                MSG( std::to_string( query.lastError( ).type( ) ) + " " + query.lastError( ).text( ).toStdString( ) ) );

            return error_detection( query.lastError( ).type( ) );
        }

        messages.push_back( UserMessage{ query.value( MESSAGE::MESSAGE_ID ).toInt( ),
                                         query.value( MESSAGE::USER_NICKNAME ).toString( ).toStdString( ),
                                         query.value( MESSAGE::MESSAGE_DATA ).toString( ).toStdString( ),
                                         query.value( MESSAGE::TIME ).toDateTime( ).toString( ).toStdString( ) } );
    }
    return IUserDataStorage::ErrorType::NO_ERROR;
}

IUserDataStorage::ErrorType QDatabase::add_message( const UserMessage& message_data )
{
    QSqlQuery query( m_chat_db );
    const bool success = query.exec( QString( "insert into %1 (%2, %3, %4) values ('%5', '%6', '%7')" )
                                         .arg( DB_MESSAGES_TABLE,
                                               DB_MESSAGES_USER_NICKNAME,
                                               DB_MESSAGES_MESSAGE,
                                               DB_MESSAGES_TIME,
                                               message_data.user_nickname.c_str( ),
                                               message_data.message.c_str( ),
                                               message_data.time.c_str( ) ) );
    if ( !success )
    {
        return error_detection( query.lastError( ).type( ) );
    }
    return IUserDataStorage::ErrorType::NO_ERROR;
}
