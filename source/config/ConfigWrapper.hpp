#ifndef CONFIGWRAPPER_HPP
#define CONFIGWRAPPER_HPP

#include <fstream>
#include <iostream>
#include <memory>
#include <string>

#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"

using namespace rapidjson;

struct ConfigData
{
    int threads_number;
    std::string log_level;
    std::string db_path;
    int minutes_inactivity;
    int days_inactivity;
    int timer_timeout;
    int history_time;
};

class ConfigWrapper
{
public:
    ConfigWrapper( const std::string& config_path );
    const ConfigData& get_config_data( ) const;

private:
    ConfigData m_config_data;
};

#endif  // CONFIGWRAPPER_HPP
