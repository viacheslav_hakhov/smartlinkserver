#include "ConfigWrapper.hpp"

namespace
{
    constexpr char const* THREADS_NUMBER = "threads_number";
    constexpr char const* LOG_LEVEL = "log_level";
    constexpr char const* DB_PATH = "db_path";
    constexpr char const* GO_AWAY_TIMEOUT = "go_away_timeout";
    constexpr char const* INACTIVITY_TIMEOUT = "inactivity_timeout";
    constexpr char const* TIMER_TIMEOUT = "timer_timeout";
    constexpr char const* HISTORY_TIME = "history_time";

    int config_to_int( const Document& doc, const char* field, int default_value )
    {
        return ( doc.HasMember( field ) ? doc[ field ].GetInt( ) : default_value );
    }

    std::string config_to_string( const Document& doc, const char* field, const char* default_value )
    {
        return ( doc.HasMember( field ) ? doc[ field ].GetString( ) : default_value );
    }
}  // namespace

ConfigWrapper::ConfigWrapper( const std::string& config_path )
{
    std::ifstream ifs{ config_path };
    if ( !ifs.is_open( ) )
    {
        std::cerr << "Could not open file for reading!\n";
        throw std::invalid_argument( "Could not open file for reading!" );
    }
    IStreamWrapper isw{ ifs };

    Document document{ };
    document.ParseStream( isw );

    m_config_data = ConfigData{ ::config_to_int( document, ::THREADS_NUMBER, 4 ),
                                ::config_to_string( document, ::LOG_LEVEL, "INFO" ),
                                ::config_to_string( document, ::DB_PATH, "../smartlinkserver/online_chat.db" ),
                                ::config_to_int( document, ::GO_AWAY_TIMEOUT, 30 ),
                                ::config_to_int( document, ::INACTIVITY_TIMEOUT, 3 ),
                                ::config_to_int( document, ::TIMER_TIMEOUT, 10000 ),
                                ::config_to_int( document, ::HISTORY_TIME, 2 ) };
}

const ConfigData& ConfigWrapper::get_config_data( ) const
{
    return m_config_data;
}
