#include "QNetwork.hpp"

#include <assert.h>

#include <QDebug>
#include <QString>
#include <algorithm>
#include <functional>

#include "Log.hpp"

namespace
{
    const size_t N_PENDING_CONNECTIONS = 20;
    const size_t PORT = 1234;
    const size_t WAIT_TIME_FOR_BYTES_WRITTEN = 10;
    const int SIZE_BUFFER{ 1000 };
}  // namespace

QNetwork::QNetwork( const ConfigData& config_data )
    : m_server( new QTcpServer( ) )
    , m_server_handler_prx( new ServerHandlerProxy( config_data, this ) )
{
    connect(
        m_server.get( ), SIGNAL( newConnection( ) ), this, SLOT( slot_connect_new_client( ) ), Qt::UniqueConnection );
    m_server->setMaxPendingConnections( ::N_PENDING_CONNECTIONS );

    if ( m_server->listen( QHostAddress::Any, ::PORT ) )
    {
        LOG( LogType::INFO, MSG( "Server is listening" ) );
    }
    else
    {
        LOG( LogType::ERROR, MSG( m_server->errorString( ).toStdString( ) ) );
    }

    connect( &m_timer, &QTimer::timeout, this, &QNetwork::on_timeout );
    m_timer.start( config_data.timer_timeout );
}

QNetwork::~QNetwork( )
{
    disconnect( m_server.get( ), SIGNAL( newConnection( ) ), this, SLOT( slot_connect_new_client( ) ) );
    disconnect( &m_timer, &QTimer::timeout, this, &QNetwork::on_timeout );
}

void send_to_client( QTcpSocket* client, const std::string& data )
{
    if ( client->isValid( ) && client->isWritable( ) )
    {
        int size_data = QByteArray::number( data.size( ) ).toInt( );
        client->write( QByteArray::number( data.size( ) ), sizeof( int ) );
        client->waitForBytesWritten( ::WAIT_TIME_FOR_BYTES_WRITTEN );
        for ( int counter_writening = 0; counter_writening < size_data; )
        {
            int size_write = ::SIZE_BUFFER < data.size( ) ? ::SIZE_BUFFER : data.size( );
            counter_writening += client->write( data.c_str( ) + counter_writening, size_write );
        }
    }
}

void send_all_clients( const std::map< QTcpSocket*, SocketDescription >& all_clients, const std::string& data )
{
    for ( const auto& [ socket, socket_description ] : all_clients )
    {
        if ( socket_description.is_logined )
        {
            send_to_client( socket, data );
        }
    }
}

void send_to_clients( const std::map< QTcpSocket*, SocketDescription >& all_clients,
                      const std::string& data,
                      size_t client )
{
    for ( const auto& [ socket, socket_description ] : all_clients )
    {
        if ( socket_description.is_logined && ( socket->socketDescriptor( ) != client ) )
        {
            send_to_client( socket, data );
        }
    }
}

std::map< QTcpSocket*, SocketDescription >::iterator find_socket( std::map< QTcpSocket*, SocketDescription >& clients,
                                                                  size_t client_descr )
{
    auto iter_client
        = std::find_if( clients.begin( ),
                        clients.end( ),
                        [ &clients, client_descr ]( std::pair< QTcpSocket* const, SocketDescription >& client_pair )
                        {
                            QTcpSocket* socket = client_pair.first;
                            return socket->socketDescriptor( ) == client_descr;
                        } );

    assert( iter_client != clients.end( ) );
    return iter_client;
}

void QNetwork::slot_send_handshake( size_t client_descr,
                                    const std::string& encoded_handshake,
                                    const bool is_successful )
{
    LOG( LogType::INFO, MSG( encoded_handshake ) );
    auto iter_client = find_socket( m_all_clients, client_descr );
    assert( iter_client->first );
    send_to_client( iter_client->first, encoded_handshake );
    if ( is_successful )
    {
        iter_client->second.is_logined = true;
        LOG( LogType::INFO, MSG( "successful" ) );
    }
    else
    {
        m_all_clients.erase( iter_client );
        LOG( LogType::INFO, MSG( " unsuccessful" ) );
    }
}

void QNetwork::slot_send_broadcast_message( size_t client_descr, const std::string& encoded_message )
{
    // send message to all clients
    auto iter_client = find_socket( m_all_clients, client_descr );
    // check if user is registered to send message
    assert( iter_client->second.is_logined && "User is not registered to send message" );
    send_to_clients( m_all_clients, encoded_message, client_descr );

    LOG( LogType::INFO, MSG( encoded_message ) );
}

void QNetwork::slot_send_history_messages( size_t client_descr, const std::string& encoded_messages )
{
    LOG( LogType::INFO, MSG( encoded_messages ) );
    auto iter_client = find_socket( m_all_clients, client_descr );
    assert( iter_client->first );
    send_to_client( iter_client->first, encoded_messages );
}

void QNetwork::slot_send_info( size_t client_descr, const std::string& encoded_data )
{
    // send info to particular client
    auto iter_client = find_socket( m_all_clients, client_descr );
    assert( iter_client->first );

    send_to_client( iter_client->first, encoded_data );
    LOG( LogType::INFO, MSG( encoded_data ) );
}

void QNetwork::slot_send_broadcast_info( const std::string& encoded_data )
{
    // send info to all clients
    send_all_clients( m_all_clients, encoded_data );
    LOG( LogType::INFO, MSG( encoded_data ) );
}

void QNetwork::slot_set_user_status( const std::string& encoded_status )
{
    send_all_clients( m_all_clients, encoded_status );
    LOG( LogType::INFO, MSG( encoded_status ) );
}

void QNetwork::slot_send_new_user_status( size_t client_descr, const std::string& encoded_status )
{
    send_to_clients( m_all_clients, encoded_status, client_descr );
    LOG( LogType::INFO, MSG( encoded_status ) );
}

void QNetwork::slot_connect_new_client( )
{
    QTcpSocket* new_socket = m_server->nextPendingConnection( );
    new_socket->setSocketOption( QAbstractSocket::KeepAliveOption, 1 );

    assert( new_socket );

    LOG( LogType::INFO, MSG( QString::number( new_socket->socketDescriptor( ) ).toStdString( ) ) );
    connect( new_socket, &QTcpSocket::readyRead, this, &QNetwork::slot_read_socket );
    connect( new_socket,
             &QTcpSocket::disconnected,
             this,
             [ this, new_socket ]( )
             {
                 LOG( LogType::INFO,
                      MSG( "Socket disconnected" + new_socket->peerAddress( ).toString( ).toStdString( ) ) );
                 m_server_handler_prx->disconnected_user_status( m_all_clients[ new_socket ].socket_descriptor );
                 m_all_clients.erase( new_socket );
                 new_socket->deleteLater( );
             } );
}

void QNetwork::add_new_client( QTcpSocket* client )
{
    auto client_iter = m_all_clients.find( client );
    if ( client_iter == m_all_clients.end( ) )
    {
        m_all_clients[ client ].is_logined = false;
        m_all_clients[ client ].socket_descriptor = client->socketDescriptor( );
    }
}

void QNetwork::slot_read_socket( )
{
    assert( m_server_handler_prx );
    LOG( LogType::INFO, MSG( "" ) );

    QTcpSocket* socket = qobject_cast< QTcpSocket* >( QObject::sender( ) );
    assert( socket != nullptr );
    add_new_client( socket );
    while ( socket->bytesAvailable( ) > 0 )
    {
        const std::string data = socket->readAll( ).toStdString( );
        m_server_handler_prx->handle_received_data( socket->socketDescriptor( ), data );
    }
}

void QNetwork::on_timeout( )
{
    m_server_handler_prx->handle_users_in_background( );
}

void QNetwork::slot_on_socket_state_changed( QAbstractSocket::SocketState state )
{
    QTcpSocket* client = qobject_cast< QTcpSocket* >( QObject::sender( ) );
    assert( client );

    QString socketIpAddress = client->peerAddress( ).toString( );
    int port = client->peerPort( );
    QString desc;
    auto socket_desc = client->socketDescriptor( );

    if ( state == QAbstractSocket::UnconnectedState )
    {
        desc = "The socket is not connected.";
    }
    else if ( state == QAbstractSocket::ConnectedState )
    {
        desc = "A connection is established.";
    }
    else if ( state == QAbstractSocket::BoundState )
    {
        desc = "The socket is bound to an address and port.";
    }
    else if ( state == QAbstractSocket::ClosingState )
    {
        desc = "The socket is about to close (data may still be waiting to be written).";
    }
    else if ( state == QAbstractSocket::ListeningState )
    {
        desc = "For internal use only.";
    }

    qDebug( ) << "Socket state changed (" + socketIpAddress + ":" + QString::number( client->socketDescriptor( ) )
                     + "): " + desc;
}
