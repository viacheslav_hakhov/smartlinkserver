#ifndef SERVERHANDLERPROXY_H
#define SERVERHANDLERPROXY_H

#include "ServerThreadPool.hpp"
#include "config/ConfigWrapper.hpp"
#include "interfaces/IServerHandler.hpp"
#include "source/TimerHandler.hpp"

class TimerHandler;
class QNetwork;

class ServerHandlerProxy : public IServerHandler
{
public:
    ServerHandlerProxy( const ConfigData& config_data, QNetwork* network );

    void handle_received_data( size_t client_descr, const std::string& data ) override;
    void handle_users_in_background( ) override;
    void disconnected_user_status( size_t socket_descriptor ) override;
    void get_all_users_activity( ) override;

    void initial_user_list( size_t client_descriptor );
    void update_last_activity_in_table( size_t client_descr, const std::string& last_time );
    void set_user_online( size_t client_descr, const std::string& user_name, const std::string& last_active );
    void recieve_all_users_activities( const std::map< std::string, std::string >& users_activities );

    void update_last_activity_time( const std::string& username, const std::string& new_last_activity ) override;
    void send_user_status_list( size_t client_descriptor, const std::string& encoded_status ) override;
    void send_broadcast_user_status_list( const std::string& encoded_status ) override;
    void send_new_user_status( size_t client_descriptor, const std::string& encoded_status ) override;

private:
    ThreadPool m_thread_pool;
    ThreadPool m_timer_pool;
    std::unique_ptr< IServerHandler > m_server_handler;
    std::unique_ptr< TimerHandler > m_timer_handler;
};

#endif  // SERVERHANDLERPROXY_H
