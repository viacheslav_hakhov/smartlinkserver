#include "ServerHandlerProxy.hpp"

#include "Log.hpp"
#include "ServerHandler.hpp"

ServerHandlerProxy::ServerHandlerProxy( const ConfigData& config_data, QNetwork* network )

    : m_thread_pool( config_data.threads_number )
    , m_timer_pool( 1 )
    , m_server_handler( new ServerHandler( config_data, network, this ) )
    , m_timer_handler( new TimerHandler( config_data, this ) )

{
}

void ServerHandlerProxy::handle_received_data( size_t client_descr, const std::string& data )

{
    LOG( LogType::INFO, MSG( "" ) );
    const auto handler
        = std::bind( &IServerHandler::handle_received_data, m_server_handler.get( ), client_descr, data );
    m_thread_pool.run( handler );
}

void ServerHandlerProxy::handle_users_in_background( )

{
    LOG( LogType::INFO, MSG( "" ) );
    const auto handler = std::bind( &TimerHandler::handle_users_in_background, m_timer_handler.get( ) );
    m_timer_pool.run( handler );
}

void ServerHandlerProxy::disconnected_user_status( size_t client_descriptor )
{
    LOG( LogType::INFO, MSG( "" ) );
    const auto handler
        = std::bind( &TimerHandler::disconnected_user_status, m_timer_handler.get( ), client_descriptor );
    m_timer_pool.run( handler );
}

void ServerHandlerProxy::update_last_activity_in_table( size_t client_descr, const std::string& last_active )
{
    LOG( LogType::INFO, MSG( "" ) );
    const auto handler
        = std::bind( &TimerHandler::update_last_activity_in_table, m_timer_handler.get( ), client_descr, last_active );
    m_timer_pool.run( handler );
}

void ServerHandlerProxy::set_user_online( size_t client_descr,
                                          const std::string& user_name,
                                          const std::string& last_active )
{
    LOG( LogType::INFO, MSG( "" ) );
    const auto handler
        = std::bind( &TimerHandler::set_user_online, m_timer_handler.get( ), client_descr, user_name, last_active );
    m_timer_pool.run( handler );
}

void ServerHandlerProxy::get_all_users_activity( )
{
    LOG( LogType::INFO, MSG( "" ) );
    const auto handler = std::bind( &IServerHandler::get_all_users_activity, m_server_handler.get( ) );
    m_thread_pool.run( handler );
}

void ServerHandlerProxy::recieve_all_users_activities( const std::map< std::string, std::string >& users_activities )
{
    LOG( LogType::INFO, MSG( "" ) );
    const auto handler = std::bind( &TimerHandler::set_initial_users_list, m_timer_handler.get( ), users_activities );
    m_timer_pool.run( handler );
}

void ServerHandlerProxy::update_last_activity_time( const std::string& username, const std::string& new_last_activity )
{
    LOG( LogType::INFO, MSG( "" ) );
    const auto handler
        = std::bind( &IServerHandler::update_last_activity_time, m_server_handler.get( ), username, new_last_activity );
    m_thread_pool.run( handler );
}

void ServerHandlerProxy::send_user_status_list( size_t client_descriptor, const std::string& encoded_status )
{
    LOG( LogType::INFO, MSG( "" ) );
    const auto handler = std::bind(
        &IServerHandler::send_user_status_list, m_server_handler.get( ), client_descriptor, encoded_status );
    m_thread_pool.run( handler );
}

void ServerHandlerProxy::send_broadcast_user_status_list( const std::string& encoded_status )
{
    LOG( LogType::INFO, MSG( "" ) );
    const auto handler
        = std::bind( &IServerHandler::send_broadcast_user_status_list, m_server_handler.get( ), encoded_status );
    m_thread_pool.run( handler );
}

void ServerHandlerProxy::send_new_user_status( size_t client_descriptor, const std::string& encoded_status )
{
    LOG( LogType::INFO, MSG( "" ) );
    const auto handler = std::bind(
        &IServerHandler::send_new_user_status, m_server_handler.get( ), client_descriptor, encoded_status );
    m_thread_pool.run( handler );
}
void ServerHandlerProxy::initial_user_list( size_t client_descriptor )
{
    LOG( LogType::INFO, MSG( "" ) );
    const auto handler = std::bind( &TimerHandler::initial_user_list, m_timer_handler.get( ), client_descriptor );
    m_timer_pool.run( handler );
}
