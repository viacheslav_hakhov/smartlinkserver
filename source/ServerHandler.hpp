#ifndef SERVERHANDLER_HPP
#define SERVERHANDLER_HPP

#include <QObject>
#include <QTcpSocket>
#include <functional>
#include <memory>
#include <string>

#include "ChatProtocol.pb.h"
#include "Log.hpp"
#include "interfaces/IServerHandler.hpp"
#include "interfaces/IUserDataStorage.hpp"
#include "source/QNetwork.hpp"
#include "source/TimerHandler.hpp"
#include "source/config/ConfigWrapper.hpp"

class ServerHandler : public QObject, public IServerHandler
{
    Q_OBJECT

public:
    ServerHandler( const ConfigData& config_data, QNetwork* network, ServerHandlerProxy* server_prx );
    ~ServerHandler( );

    void handle_received_data( size_t client_descr, const std::string& data ) override;
    void get_all_users_activity( ) override;
    void update_last_activity_time( const std::string& username, const std::string& new_last_activity ) override;
    void send_user_status_list( size_t client_descriptor, const std::string& encoded_status ) override;
    void send_broadcast_user_status_list( const std::string& encoded_status ) override;
    void send_new_user_status( size_t client_descriptor, const std::string& encoded_status ) override;

signals:
    void signal_send_handshake( size_t client_descr, const std::string& encoded_handshake, const bool );
    void signal_send_registration( size_t client_descr, const std::string& encoded_registration, const bool );
    void signal_send_broadcast_message( size_t client_descr, const std::string& encoded_message );
    void signal_send_history_messages( size_t client_descr, const std::string& encoded_messages );
    void signal_send_info( size_t client_descr, const std::string& encoded_info );
    void signal_send_broadcast_info( const std::string& encoded_info );
    void signal_set_user_status( const std::string& encoded_status );
    void signal_send_new_user_status( size_t client_descr, const std::string& encoded_status );

private:
    void on_handshake_received( size_t client_descr,
                                int64_t time_stamp,
                                const Protocol::UserCredentialRequest& user_credential );
    void on_registration_received( size_t client_descr,
                                   int64_t time_stamp,
                                   const Protocol::UserFullData& user_full_data );
    void on_broadcast_message_received( size_t client_descr,
                                        int64_t time_stamp,
                                        const Protocol::UserMessageRequest& user_message );
    void on_message_history_received( size_t client_descr,
                                      int64_t time_stamp,
                                      const Protocol::MessagesRequest& message_request );

    void on_users_list_received( size_t client_descr );

    void initial_user_list( );
    void set_initial_users_list( );
    std::string update_users_statuses( bool is_initial_list );
    std::string get_user_name_by_descriptor( size_t client_descriptor );

private:
    QNetwork* m_network;
    ServerHandlerProxy* m_server_prx;
    std::shared_ptr< TimerHandler > m_timer_handler;
    std::shared_ptr< IUserDataStorage > m_user_storage;
};

#endif  // SERVER_HPP
