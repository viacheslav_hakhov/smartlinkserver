#include "TimerHandler.hpp"

namespace
{
    std::time_t get_current_time( )
    {
        std::time_t current_time = std::time( nullptr );
        std::tm* gm_time = std::localtime( &current_time );
        return std::mktime( gm_time );
    }

    std::time_t strtime_to_timet( const std::string& str_time )
    {
        std::tm tm{ };
        std::istringstream str_stream( str_time );
        str_stream >> std::get_time( &tm, "%Y-%m-%d %T" );
        return std::mktime( &tm ) - 3600;
    }

    std::string generate_time( )
    {
        std::time_t current_time = std::time( nullptr );
        std::tm* gm_time = std::localtime( &current_time );
        std::stringstream str_time;
        str_time << std::put_time( gm_time, "%Y-%m-%d %T" );
        return str_time.str( );
    }

    std::string generate_response( const Protocol::Response_TypeResponse& type_response,
                                   const std::string& response_data,
                                   int64_t time_stamp = 0 )
    {
        Protocol::Response response;

        switch ( type_response )
        {
            case Protocol::Response_TypeResponse_BROADCAST_INFO:
            case Protocol::Response_TypeResponse_BROADCAST_USERS_UPDATE_LIST:
            case Protocol::Response_TypeResponse_BROADCAST_MESSAGE:
                response.set_type_response( type_response );
                break;

            case Protocol::Response_TypeResponse_BROADCAST_USERS_LIST:
                response.set_type_response( type_response );
                response.set_time_stamp( time_stamp );
                break;

            default:
                response.set_time_stamp( time_stamp );
                break;
        }

        response.set_response_data( response_data );

        return response.SerializeAsString( );
    }

}  // namespace

TimerHandler::TimerHandler( const ConfigData& config_data, ServerHandlerProxy* server_prx )
    : m_server_prx( server_prx )
    , m_timer_config( config_data.minutes_inactivity, config_data.days_inactivity )
{
    get_all_users_activity( );
}

void TimerHandler::get_all_users_activity( )
{
    m_server_prx->get_all_users_activity( );
}

void TimerHandler::set_initial_users_list( const std::map< std::string, std::string >& users_activities )
{
    for ( auto& [ user_name, last_activity ] : users_activities )
    {
        m_users_status[ { 0, user_name } ] = { last_activity, Protocol::UserStatus_Status_OFFLINE };
    }
}

void TimerHandler::initial_user_list( size_t client_descriptor )
{
    const std::string encoded_user_list = update_users_statuses( true );
    LOG( LogType::INFO, MSG( encoded_user_list ) );

    const std::string encoded_status
        = ::generate_response( Protocol::Response_TypeResponse_BROADCAST_USERS_LIST, encoded_user_list );
    m_server_prx->send_user_status_list( client_descriptor, encoded_status );
}

void TimerHandler::update_last_activity_in_table( size_t client_descr, const std::string& last_time )
{
    std::string user_name = get_user_name_by_descriptor( client_descr );
    LOG( LogType::INFO, MSG( user_name.c_str( ) ) );

    auto current_user_status = m_users_status.find( { client_descr, user_name } );
    if ( current_user_status != m_users_status.end( ) )
    {
        LOG( LogType::INFO, MSG( current_user_status->second.last_activity_time ) );
        current_user_status->second.last_activity_time = last_time;
    }
    else
    {
        LOG( LogType::ERROR, MSG( "" ) );
    }
}

void TimerHandler::handle_users_in_background( )
{
    LOG( LogType::INFO, MSG( "TODO TIMER BG STATUSES HANDLE" ) );
    const std::string encoded_user_list = update_users_statuses( false );
    LOG( LogType::INFO, MSG( encoded_user_list ) );

    const std::string encoded_status
        = ::generate_response( Protocol::Response_TypeResponse_BROADCAST_USERS_UPDATE_LIST, encoded_user_list );
    m_server_prx->send_broadcast_user_status_list( encoded_status );
}

std::string TimerHandler::update_users_statuses( bool is_initial_list )
{
    std::time_t current_time_t = get_current_time( );
    Protocol::UsersList* user_update_list = new Protocol::UsersList;
    for ( auto& [ user_status_key, user_status_value ] : m_users_status )
    {
        std::time_t last_active_time = strtime_to_timet( user_status_value.last_activity_time );
        time_t difference = current_time_t - last_active_time;

        Protocol::UserStatus::Status current_status;
        if ( user_status_value.previous_status == Protocol::UserStatus_Status_OFFLINE )
        {
            if ( difference >= m_timer_config.INACTIVE_TIME_IN_SECONDS )
            {
                current_status = Protocol::UserStatus_Status_INACTIVE;
            }
            else
            {
                current_status = user_status_value.previous_status;
            }
        }
        else
        {
            if ( difference < m_timer_config.GO_AWAY_TIME_IN_SECONDS )
            {
                current_status = Protocol::UserStatus_Status_ONLINE;
            }
            else if ( difference >= m_timer_config.GO_AWAY_TIME_IN_SECONDS
                      && difference < m_timer_config.INACTIVE_TIME_IN_SECONDS )
            {
                current_status = Protocol::UserStatus_Status_GO_AWAY;
            }
            else
            {
                current_status = user_status_value.previous_status;
            }
        }

        if ( is_initial_list || ( !is_initial_list && user_status_value.previous_status != current_status ) )
        {
            Protocol::UserStatus* user_status = user_update_list->add_user_status( );
            user_status->set_user_name( user_status_key.user_name );
            user_status->set_status( current_status );
        }
        user_status_value.previous_status = current_status;
    }
    return user_update_list->SerializeAsString( );
}

std::string TimerHandler::get_user_name_by_descriptor( size_t client_descriptor )
{
    auto iter_client
        = std::find_if( m_users_status.begin( ),
                        m_users_status.end( ),
                        [ client_descriptor ]( std::pair< UserStatusKey const, UserStatusValue >& client_pair )
                        { return client_pair.first.socket_descriptor == client_descriptor; } );

    return iter_client != m_users_status.end( ) ? iter_client->first.user_name : "";
}

void TimerHandler::disconnected_user_status( size_t client_descriptor )
{
    std::string user_name = get_user_name_by_descriptor( client_descriptor );
    LOG( LogType::INFO, MSG( user_name.c_str( ) ) );

    auto current_user_status = m_users_status.find( { client_descriptor, user_name } );

    if ( current_user_status != m_users_status.end( ) )
    {
        const std::string username = current_user_status->first.user_name;

        Protocol::UsersList* user_update_list = new Protocol::UsersList;
        Protocol::UserStatus* user_status = user_update_list->add_user_status( );
        user_status->set_user_name( username );
        user_status->set_status( Protocol::UserStatus_Status_OFFLINE );
        const std::string encoded_status = ::generate_response(
            Protocol::Response_TypeResponse_BROADCAST_USERS_UPDATE_LIST, user_update_list->SerializeAsString( ) );

        std::string new_last_activity = generate_time( );
        m_server_prx->update_last_activity_time( username, new_last_activity );

        current_user_status->second.last_activity_time = new_last_activity;
        current_user_status->second.previous_status = Protocol::UserStatus_Status_OFFLINE;
        m_server_prx->send_broadcast_user_status_list( encoded_status );
    }
}

void TimerHandler::set_user_online( size_t client_descr, const std::string& user_name, const std::string& last_active )
{
    m_users_status[ { client_descr, user_name } ] = { last_active, Protocol::UserStatus_Status_ONLINE };
    Protocol::UsersList* user_update_list = new Protocol::UsersList;
    Protocol::UserStatus* user_status = user_update_list->add_user_status( );
    user_status->set_user_name( user_name );
    user_status->set_status( Protocol::UserStatus_Status_ONLINE );
    const std::string encoded_status = ::generate_response( Protocol::Response_TypeResponse_BROADCAST_USERS_UPDATE_LIST,
                                                            user_update_list->SerializeAsString( ) );
    m_server_prx->send_new_user_status( client_descr, encoded_status );
}
