#include "ServerThreadPool.hpp"

#include "Log.hpp"

ThreadPool::ThreadPool( std::size_t thread_count )
{
    if ( !thread_count )
    {
        LOG( LogType::ERROR, "Bad thread count! Must be non-zero!" );
        thread_count = 1;
    }

    m_threads.reserve( thread_count );
    for ( auto i = 0; i < thread_count; ++i )
    {
        m_threads.push_back( std::thread(
            [ this ]( )
            {
                while ( true )
                {
                    std::unique_ptr< work_item_t > work{ nullptr };
                    {
                        std::unique_lock guard( m_queue_lock );
                        m_condition.wait( guard, [ & ]( ) { return !m_queue.empty( ); } );

                        work = std::move( m_queue.front( ) );
                        m_queue.pop( );
                    }
                    if ( !work )
                    {
                        break;
                    }
                    ( *work )( );
                }
            } ) );
    }
    LOG( LogType::INFO, MSG( "" ) );
}

ThreadPool::~ThreadPool( )
{
    {
        std::unique_lock guard( m_queue_lock );
        for ( auto& t : m_threads )
        {
            m_queue.push( std::unique_ptr< work_item_t >{ nullptr } );
            m_condition.notify_one( );
        }
    }
    for ( auto& t : m_threads )
    {
        t.join( );
    }
}

void ThreadPool::run( work_item_t work_item )
{
    auto unq_work_item = std::make_unique< work_item_t >( std::move( work_item ) );
    {
        std::unique_lock guard( m_queue_lock );
        m_queue.push( std::move( unq_work_item ) );
    }
    m_condition.notify_one( );
}
