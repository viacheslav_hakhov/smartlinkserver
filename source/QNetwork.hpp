#ifndef QNETWORK_HPP
#define QNETWORK_HPP

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QTimer>
#include <map>
#include <memory>
#include <string>

#include "ServerHandlerProxy.hpp"
#include "config/ConfigWrapper.hpp"
#include "interfaces/INetwork.hpp"
#include "interfaces/IServerHandler.hpp"

struct SocketDescription
{
    explicit SocketDescription( )
        : socket_descriptor( 0 )
        , is_logined( false )
    {
    }

    explicit SocketDescription( size_t socket_desc )
        : socket_descriptor( socket_desc )
        , is_logined( false )
    {
    }

    SocketDescription( bool is_logined_, size_t socket_desc )
        : is_logined( is_logined_ )
        , socket_descriptor( socket_desc )

    {
    }

    bool is_logined;
    size_t socket_descriptor;
};

class QNetwork : public QObject, public INetwork
{
    Q_OBJECT
public:
    QNetwork( const ConfigData& config_data );
    ~QNetwork( );

public slots:
    void slot_connect_new_client( ) override;
    void slot_read_socket( ) override;
    void slot_on_socket_state_changed( QAbstractSocket::SocketState state );
    void slot_send_handshake( size_t client_descr,
                              const std::string& encoded_handshake,
                              const bool is_successful ) override;
    void slot_send_broadcast_message( size_t client_descr, const std::string& encoded_message ) override;
    void slot_send_history_messages( size_t client_descr, const std::string& encoded_messages ) override;
    void slot_send_info( size_t client_descr, const std::string& encoded_info ) override;
    void slot_send_broadcast_info( const std::string& encoded_info ) override;
    void slot_set_user_status( const std::string& encoded_status ) override;
    void slot_send_new_user_status( size_t client_descr, const std::string& encoded_status ) override;

private:
    void add_new_client( QTcpSocket* client );

private slots:
    void on_timeout( );

private:
    std::unique_ptr< QTcpServer > m_server;
    std::map< QTcpSocket*, SocketDescription > m_all_clients;
    std::shared_ptr< IServerHandler > m_server_handler_prx;
    QTimer m_timer;
};

#endif  // QNETWORK_HPP
