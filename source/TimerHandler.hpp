#ifndef TIMERHANDLER_HPP
#define TIMERHANDLER_HPP

#include <assert.h>

#include <ctime>
#include <iomanip>
#include <memory>
#include <sstream>
#include <string>

#include "ChatProtocol.pb.h"
#include "Log.hpp"
#include "source/ServerHandlerProxy.hpp"
#include "source/config/ConfigWrapper.hpp"

class ServerHandlerProxy;

struct UserStatusKey
{
    explicit UserStatusKey( size_t socket_desc )
        : socket_descriptor( socket_desc )
        , user_name( "" )
    {
    }

    UserStatusKey( size_t socket_desc, const std::string& username )
        : socket_descriptor( socket_desc )
        , user_name( username )
    {
    }

    size_t socket_descriptor;
    std::string user_name;
};

struct UserStatusValue
{
    std::string last_activity_time;
    Protocol::UserStatus::Status previous_status;
};

struct UserStatusTimeConfig
{
    static const int MINUTES_TO_SECONDS = 60;
    static const int DAYS_TO_SECONDS = 24 * 60 * 60;

    UserStatusTimeConfig( int go_away, int inactive )
        : GO_AWAY_TIME_IN_SECONDS( go_away * MINUTES_TO_SECONDS )
        , INACTIVE_TIME_IN_SECONDS( inactive * DAYS_TO_SECONDS )
    {
    }

    const time_t GO_AWAY_TIME_IN_SECONDS;
    const time_t INACTIVE_TIME_IN_SECONDS;
};

class TimerHandler
{
    struct UserStatusComparator
    {
        bool operator( )( const UserStatusKey& l, const UserStatusKey& r ) const
        {
            LOG( LogType::INFO, MSG( " comparator " ) );
            return l.socket_descriptor < r.socket_descriptor
                   || ( l.socket_descriptor == r.socket_descriptor && ( l.user_name < r.user_name ) );
        }
    };

public:
    TimerHandler( const ConfigData& config_data, ServerHandlerProxy* server_prx );

    void initial_user_list( size_t client_descriptor );

    void update_last_activity_in_table( size_t client_descr, const std::string& last_time );

    void handle_users_in_background( );

    std::string update_users_statuses( bool is_initial_list );

    std::string get_user_name_by_descriptor( size_t client_descriptor );

    void set_user_online( size_t client_descr, const std::string& user_name, const std::string& last_active );

    void disconnected_user_status( size_t client_descriptor );

    void get_all_users_activity( );

    void set_initial_users_list( const std::map< std::string, std::string >& users_activities );

private:
    std::map< UserStatusKey, UserStatusValue, UserStatusComparator > m_users_status;
    ServerHandlerProxy* m_server_prx;
    UserStatusTimeConfig m_timer_config;
};

#endif  // TIMERHANDLER_HPP
