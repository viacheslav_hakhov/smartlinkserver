#ifndef QDATABASE_HPP
#define QDATABASE_HPP
#include <QDateTime>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlTableModel>
#include <mutex>

#include "interfaces/IUserDataStorage.hpp"

class QDatabase : public IUserDataStorage
{
public:
    QDatabase( const std::string& db_filename, int history_time );
    ~QDatabase( );

    // table users
    ErrorType get_user_data( const std::string& nickname, UserInfo& user_info ) override;
    ErrorType get_user_data( int id, UserInfo& user_info ) override;
    ErrorType get_last_activity( const std::string& nickname, std::string& last_activity ) override;
    ErrorType get_all_last_activities( std::map< std::string, std::string >& users_activities ) override;
    ErrorType update_last_activity_time( const std::string& username,
                                         const std::string& new_last_activity_time ) override;
    ErrorType add_user( const UserInfo& user_data ) override;
    ErrorType update_user( const UserInfo& user_data ) override;

    // table messages
    ErrorType get_messages( const std::string& datetime_from,
                            const std::string& datetime_to,
                            std::vector< UserMessage >& messages ) override;
    ErrorType get_messages( std::vector< UserMessage >& messages, const std::string& timestamp_from ) override;
    ErrorType get_messages( std::vector< UserMessage >& messages ) override;
    ErrorType get_messages( const std::string& day_data, std::vector< UserMessage >& messages ) override;
    ErrorType add_message( const UserMessage& message_data ) override;

private:
    QSqlDatabase m_chat_db;
    QSqlTableModel m_users_table;
    QSqlTableModel m_messages_table;
    const int m_history_time;
};

#endif  // QDATABASE_HPP
