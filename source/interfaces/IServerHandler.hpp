#ifndef ISERVERHANDLER_H
#define ISERVERHANDLER_H

#include <QTcpSocket>
#include <string>

class IServerHandler
{
public:
    virtual ~IServerHandler( ) = default;

    virtual void handle_received_data( size_t client_descr, const std::string& data ) = 0;
    virtual void handle_users_in_background( )
    {
    }
    virtual void disconnected_user_status( size_t socket_descriptor )
    {
    }
    virtual void get_all_users_activity( ) = 0;
    virtual void update_last_activity_time( const std::string& username, const std::string& new_last_activity ) = 0;
    virtual void send_user_status_list( size_t client_descriptor, const std::string& encoded_status ) = 0;
    virtual void send_broadcast_user_status_list( const std::string& encoded_status ) = 0;
    virtual void send_new_user_status( size_t client_descriptor, const std::string& encoded_status ) = 0;
};
#endif  // ISERVER_H
