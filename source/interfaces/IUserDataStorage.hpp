#ifndef IDATABASE_HPP
#define IDATABASE_HPP

#include <map>
#include <memory>
#include <string>
#include <vector>

struct UserInfo
{
    UserInfo( ) = default;

    UserInfo( const std::string& nick,
              const std::string& name_,
              const std::string& surname_,
              const std::string& pass,
              const std::string& phone )
        : nickname( nick )
        , name( name_ )
        , surname( surname_ )
        , password( pass )
        , phone_number( phone )
    {
    }
    UserInfo( int id_,
              const std::string& nick,
              const std::string& name_,
              const std::string& surname_,
              const std::string& pass,
              const std::string& phone,
              const std::string& reg_time,
              const std::string& last_activity_ )
        : id( id_ )
        , nickname( nick )
        , name( name_ )
        , surname( surname_ )
        , password( pass )
        , phone_number( phone )
        , registration_time( reg_time )
        , last_activity( last_activity_ )
    {
    }

    int id;
    std::string nickname;
    std::string name;
    std::string surname;
    std::string password;
    std::string phone_number;
    std::string registration_time;
    std::string last_activity;

    bool is_empty( ) const
    {
        return ( nickname.empty( ) && name.empty( ) && surname.empty( ) );
    }
};

struct UserMessage
{
    UserMessage( ) = default;

    UserMessage( const std::string& nickname, const std::string& msg )
        : user_nickname( nickname )
        , message( msg )
    {
    }
    UserMessage( const std::string& nickname, const std::string& msg, const std::string& time_ )
        : user_nickname( nickname )
        , message( msg )
        , time( time_ )
    {
    }
    UserMessage( int id_, const std::string& nickname, const std::string& msg, const std::string& time_ )
        : id( id_ )
        , user_nickname( nickname )
        , message( msg )
        , time( time_ )
    {
    }

    int id;
    std::string user_nickname;
    std::string message;
    std::string time;

    bool is_empty( ) const
    {
        return ( user_nickname.empty( ) && message.empty( ) );
    }
};

class IUserDataStorage
{
public:
    enum ErrorType
    {
        CONNECTION_ERROR,
        SYNTAX_ERROR,
        RECORD_ERROR,
        RECORD_EXIST,
        UNKNOWN_ERROR,
        NO_ERROR
    };

    static std::shared_ptr< IUserDataStorage > create_data_storage( const std::string& db_filename, int history_time );
    virtual ~IUserDataStorage( ) = default;

    // table users
    virtual ErrorType get_user_data( const std::string& nickname, UserInfo& user_info ) = 0;
    virtual ErrorType get_user_data( int id, UserInfo& user_info ) = 0;
    virtual ErrorType get_last_activity( const std::string& nickname, std::string& last_activity ) = 0;
    virtual ErrorType update_last_activity_time( const std::string& username,
                                                 const std::string& new_last_activity_time )
        = 0;
    virtual ErrorType get_all_last_activities( std::map< std::string, std::string >& users_activities ) = 0;
    virtual ErrorType add_user( const UserInfo& user_data ) = 0;
    virtual ErrorType update_user( const UserInfo& user_data ) = 0;

    // table messages
    virtual ErrorType get_messages( const std::string& datetime_from,
                                    const std::string& datetime_to,
                                    std::vector< UserMessage >& messages )
        = 0;
    virtual ErrorType get_messages( const std::string& day_data, std::vector< UserMessage >& messages ) = 0;
    virtual ErrorType get_messages( std::vector< UserMessage >& messages ) = 0;
    virtual ErrorType get_messages( std::vector< UserMessage >& messages, const std::string& timestamp_from ) = 0;
    virtual ErrorType add_message( const UserMessage& message_data ) = 0;
};

#endif  // IDATABASE_HPP
