#ifndef INETWORK_HPP
#define INETWORK_HPP

#include <string>

class INetwork
{
public:
    virtual ~INetwork( ) = default;

    virtual void slot_connect_new_client( ) = 0;
    virtual void slot_read_socket( ) = 0;
    virtual void slot_send_handshake( size_t client_descr,
                                      const std::string& encoded_handshake,
                                      const bool is_successful )
        = 0;
    virtual void slot_send_broadcast_message( size_t client_descr, const std::string& encoded_message ) = 0;
    virtual void slot_send_history_messages( size_t client_descr, const std::string& encoded_messages ) = 0;
    virtual void slot_send_info( size_t client_descr, const std::string& encoded_info ) = 0;
    virtual void slot_send_broadcast_info( const std::string& encoded_info ) = 0;
    virtual void slot_set_user_status( const std::string& encoded_status ) = 0;
    virtual void slot_send_new_user_status( size_t client_descr, const std::string& encoded_message ) = 0;
};

#endif  // INETWORK_HPP
