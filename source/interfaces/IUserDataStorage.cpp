#include "IUserDataStorage.hpp"

#include "../QDatabase.hpp"

std::shared_ptr< IUserDataStorage > IUserDataStorage::create_data_storage( const std::string& db_path,
                                                                           int history_time )
{
    return std::shared_ptr< IUserDataStorage >{ new QDatabase( db_path, history_time ) };
}
