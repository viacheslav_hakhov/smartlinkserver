#include "ServerHandler.hpp"

#include <assert.h>

#include <ctime>
#include <iomanip>
#include <sstream>

#include "Log.hpp"
#include "interfaces/INetwork.hpp"
#include "interfaces/IUserDataStorage.hpp"

namespace
{
    const std::string HANDSHAKE_INFO_COLOR = "Green";

    std::string generate_time( )
    {
        std::time_t current_time = std::time( nullptr );
        std::tm* gm_time = std::localtime( &current_time );
        std::stringstream str_time;
        str_time << std::put_time( gm_time, "%Y-%m-%d %T" );
        return str_time.str( );
    }

    std::string generate_login_response_data( const Protocol::UserCredentialRequest& user_credential,
                                              bool is_valid_username,
                                              const std::string& detail_info )
    {
        Protocol::ServerResponse server_client_response;
        server_client_response.set_id_hmi( user_credential.id_hmi( ) );
        server_client_response.set_status_code( is_valid_username ? Protocol::SUCCESS
                                                                  : Protocol::ERROR_USER_CREDENTIAL );
        server_client_response.set_detail_info( detail_info );

        return server_client_response.SerializeAsString( );
    }

    std::string generate_registration_response_data( const Protocol::UserFullData& user_full_data,
                                                     const Protocol::StatusCode& status_code,
                                                     const std::string& detail_info = "" )
    {
        Protocol::ServerResponse server_client_response;
        server_client_response.set_id_hmi( user_full_data.user_data( ).id_hmi( ) );
        server_client_response.set_status_code( status_code );
        server_client_response.set_detail_info( detail_info );

        return server_client_response.SerializeAsString( );
    }

    std::string generate_response( const Protocol::Response_TypeResponse& type_response,
                                   const std::string& response_data,
                                   int64_t time_stamp = 0 )
    {
        Protocol::Response response;

        switch ( type_response )
        {
            case Protocol::Response_TypeResponse_BROADCAST_INFO:
            case Protocol::Response_TypeResponse_BROADCAST_USERS_UPDATE_LIST:
            case Protocol::Response_TypeResponse_BROADCAST_MESSAGE:
            case Protocol::Response_TypeResponse_BROADCAST_USERS_LIST:
            case Protocol::Response_TypeResponse_UPDATE_MESSAGE_STATUS:
                response.set_type_response( type_response );
                break;

            case Protocol::Response_TypeResponse_MESSAGE_HISTORY:
                response.set_type_response( type_response );
                response.set_time_stamp( time_stamp );

            default:
                response.set_time_stamp( time_stamp );
                break;
        }

        response.set_response_data( response_data );

        return response.SerializeAsString( );
    }

}  // namespace

ServerHandler::ServerHandler( const ConfigData& config_data, QNetwork* network, ServerHandlerProxy* server_prx )
    : m_network( network )
    , m_user_storage( IUserDataStorage::create_data_storage( config_data.db_path, config_data.history_time ) )
    , m_server_prx( server_prx )
{
    assert( m_network );
    connect( this, &ServerHandler::signal_send_handshake, m_network, &QNetwork::slot_send_handshake );
    connect( this, &ServerHandler::signal_send_broadcast_message, m_network, &QNetwork::slot_send_broadcast_message );
    connect( this, &ServerHandler::signal_send_info, m_network, &QNetwork::slot_send_info );
    connect( this, &ServerHandler::signal_send_broadcast_info, m_network, &QNetwork::slot_send_broadcast_info );
    connect( this, &ServerHandler::signal_set_user_status, m_network, &QNetwork::slot_set_user_status );
    connect( this, &ServerHandler::signal_send_new_user_status, m_network, &QNetwork::slot_send_new_user_status );
    connect( this, &ServerHandler::signal_send_history_messages, m_network, &QNetwork::slot_send_history_messages );
}

ServerHandler::~ServerHandler( )
{
}

void ServerHandler::handle_received_data( size_t client_descr, const std::string& data )
{
    assert( m_network );
    LOG( LogType::INFO, MSG( QString::number( client_descr ).toStdString( ) + data ) );

    Protocol::Request request_package;
    request_package.ParseFromString( data );

    switch ( request_package.type_request( ) )
    {
        case Protocol::Request_TypeRequest_USER_REGISTRATION:
        {
            Protocol::UserFullData user_full_data;
            user_full_data.ParseFromString( request_package.request_data( ) );
            on_registration_received( client_descr, request_package.time_stamp( ), user_full_data );
            break;
        }

        case Protocol::Request_TypeRequest_USER_CREDENTINAL:
        {
            Protocol::UserCredentialRequest user_credentinal;
            user_credentinal.ParseFromString( request_package.request_data( ) );
            on_handshake_received( client_descr, request_package.time_stamp( ), user_credentinal );
            break;
        }

        case Protocol::Request_TypeRequest_USER_MESSAGE:
        {
            Protocol::UserMessageRequest user_message;
            user_message.ParseFromString( request_package.request_data( ) );
            on_broadcast_message_received( client_descr, request_package.time_stamp( ), user_message );
            break;
        }

        case Protocol::Request_TypeRequest_MESSAGE_HISTORY:
        {
            Protocol::MessagesRequest message_request;
            message_request.ParseFromString( request_package.request_data( ) );
            on_message_history_received( client_descr, request_package.time_stamp( ), message_request );
            break;
        }

        default:
            break;
    }
}

void ServerHandler::get_all_users_activity( )
{
    std::map< std::string, std::string > users_activities;
    m_user_storage->get_all_last_activities( users_activities );
    m_server_prx->recieve_all_users_activities( users_activities );
}

namespace
{
    std::time_t get_current_time( )
    {
        std::time_t current_time = std::time( nullptr );
        std::tm* gm_time = std::localtime( &current_time );
        return std::mktime( gm_time );
    }

    std::time_t strtime_to_timet( const std::string& str_time )
    {
        std::tm tm{ };
        std::istringstream str_stream( str_time );
        str_stream >> std::get_time( &tm, "%Y-%m-%d %T" );
        return std::mktime( &tm );
    }

}  // namespace

void ServerHandler::on_handshake_received( size_t client_descr,
                                           int64_t time_stamp,
                                           const Protocol::UserCredentialRequest& user_credential )
{
    assert( m_network );
    LOG( LogType::INFO, MSG( user_credential.user_name( ) ) );

    UserInfo user_data{ };
    IUserDataStorage::ErrorType error = m_user_storage->get_user_data( user_credential.user_name( ), user_data );

    std::string encoded_handshake;

    if ( error == IUserDataStorage::ErrorType::NO_ERROR )
    {
        const std::string last_active_time = generate_time( );
        m_user_storage->update_last_activity_time( user_credential.user_name( ), last_active_time );
        m_server_prx->update_last_activity_in_table( client_descr, last_active_time );
        const bool is_valid_username = ( !user_data.is_empty( ) && user_credential.password( ) == user_data.password );
        const std::string detail_info = ( is_valid_username ) ? " - authorized successfully"
                                                              : "Username or password is not valid. Try once again)";

        std::string client_server_response_data
            = ::generate_login_response_data( user_credential, is_valid_username, detail_info );

        encoded_handshake
            = ::generate_response( Protocol::Response_TypeResponse_NONE, client_server_response_data, time_stamp );

        if ( is_valid_username )
        {
            const std::string new_user_added_info = "<font color=\"" + ::HANDSHAKE_INFO_COLOR + "\">"
                                                    + user_credential.user_name( )
                                                    + " has joined the chat room.</font>";
            Protocol::Info broadcast_info;
            broadcast_info.set_detail_info( new_user_added_info );

            const std::string encoded_info = ::generate_response( Protocol::Response_TypeResponse_BROADCAST_INFO,
                                                                  broadcast_info.SerializeAsString( ) );
            emit signal_send_broadcast_info( encoded_info );
        }
        m_server_prx->set_user_online( client_descr, user_credential.user_name( ), last_active_time );
        emit signal_send_handshake( client_descr, encoded_handshake, is_valid_username );

        if ( is_valid_username )
        {
            on_users_list_received( client_descr );
        }
    }
}

void ServerHandler::on_registration_received( size_t client_descr,
                                              int64_t time_stamp,
                                              const Protocol::UserFullData& user_full_data )
{
    assert( m_network );
    LOG( LogType::INFO, MSG( user_full_data.user_data( ).user_name( ) ) );
    LOG( LogType::INFO, MSG( user_full_data.password( ) ) );

    UserInfo user_data( user_full_data.user_data( ).user_name( ),
                        user_full_data.user_data( ).name( ),
                        user_full_data.user_data( ).surname( ),
                        user_full_data.password( ),
                        user_full_data.user_data( ).phone_number( ) );
    IUserDataStorage::ErrorType error = m_user_storage->add_user( user_data );

    std::string encoded_registration, client_server_response_data;

    if ( error == IUserDataStorage::ErrorType::NO_ERROR )
    {
        client_server_response_data = ::generate_registration_response_data( user_full_data, Protocol::SUCCESS );

        const std::string new_user_added_info = "<font color=\"" + ::HANDSHAKE_INFO_COLOR + "\">"
                                                + user_full_data.user_data( ).user_name( )
                                                + " has joined the chat room.</font>";
        Protocol::Info broadcast_info;
        broadcast_info.set_detail_info( new_user_added_info );

        const std::string encoded_info = ::generate_response( Protocol::Response_TypeResponse_BROADCAST_INFO,
                                                              broadcast_info.SerializeAsString( ) );
        emit signal_send_broadcast_info( encoded_info );

        m_server_prx->set_user_online( client_descr, user_full_data.user_data( ).user_name( ), generate_time( ) );
    }
    else if ( error == IUserDataStorage::ErrorType::RECORD_EXIST )
    {
        client_server_response_data = ::generate_registration_response_data(
            user_full_data, Protocol::USER_EXISTS, "User with such nickname already exists ;)" );
    }

    encoded_registration
        = ::generate_response( Protocol::Response_TypeResponse_NONE, client_server_response_data, time_stamp );

    emit signal_send_handshake(
        client_descr, encoded_registration, error == IUserDataStorage::ErrorType::NO_ERROR ? true : false );

    if ( error == IUserDataStorage::ErrorType::NO_ERROR )
    {
        on_users_list_received( client_descr );
    }
}

void ServerHandler::on_broadcast_message_received( size_t client_descr,
                                                   int64_t time_stamp,
                                                   const Protocol::UserMessageRequest& user_message )
{
    assert( m_network );
    LOG( LogType::INFO, MSG( user_message.user_name( ) + " " + user_message.data( ) ) );

    // check if user is registered to send message
    UserInfo user_data{ };
    IUserDataStorage::ErrorType error = m_user_storage->get_user_data( user_message.user_name( ), user_data );

    if ( error == IUserDataStorage::ErrorType::NO_ERROR )
    {
        if ( !user_data.is_empty( ) )
        {
            std::string server_time = generate_time( );
            m_user_storage->add_message( UserMessage( user_message.user_name( ), user_message.data( ), server_time ) );
            m_user_storage->update_last_activity_time( user_message.user_name( ), server_time );
            m_server_prx->update_last_activity_in_table( client_descr, server_time );
            Protocol::BroadcastMessageResponse message_response;
            message_response.set_user_name( user_message.user_name( ) );
            message_response.set_data( user_message.data( ) );
            message_response.set_date_time( server_time );

            std::string encoded_message_for_recievers = ::generate_response(
                Protocol::Response_TypeResponse_BROADCAST_MESSAGE, message_response.SerializeAsString( ) );
            emit signal_send_broadcast_message( client_descr, encoded_message_for_recievers );

            std::string encoded_message_for_sender = ::generate_response(
                Protocol::Response_TypeResponse_UPDATE_MESSAGE_STATUS, message_response.SerializeAsString( ) );
            emit signal_send_info( client_descr, encoded_message_for_sender );
        }
    }
}

void ServerHandler::on_message_history_received( size_t client_descr,
                                                 int64_t time_stamp,
                                                 const Protocol::MessagesRequest& messages_request )
{
    assert( m_network );
    LOG( LogType::INFO, MSG( messages_request.user_name( ) ) );

    // check if user is registered to send message
    UserInfo user_data{ };
    IUserDataStorage::ErrorType error = m_user_storage->get_user_data( messages_request.user_name( ), user_data );

    if ( error == IUserDataStorage::ErrorType::NO_ERROR )
    {
        if ( !user_data.is_empty( ) )
        {
            std::vector< UserMessage > messages_history;
            if ( !messages_request.timestamp_from( ).empty( ) )
            {
                m_user_storage->get_messages( messages_history, messages_request.timestamp_from( ) );
            }
            else
            {
                m_user_storage->get_messages( messages_history );
            }
            std::string server_time = generate_time( );
            m_user_storage->update_last_activity_time( user_data.nickname, server_time );
            m_server_prx->update_last_activity_in_table( client_descr, server_time );
            Protocol::MessagesResponse* messages_response = new Protocol::MessagesResponse;
            for ( auto& user_message : messages_history )
            {
                Protocol::BroadcastMessageResponse* message_response = messages_response->add_messages( );
                message_response->set_user_name( user_message.user_nickname );
                message_response->set_data( user_message.message );
                message_response->set_date_time( user_message.time );
            }

            std::string encoded_messages = ::generate_response(
                Protocol::Response_TypeResponse_MESSAGE_HISTORY, messages_response->SerializeAsString( ), time_stamp );

            emit signal_send_history_messages( client_descr, encoded_messages );
        }
    }
}

void ServerHandler::on_users_list_received( size_t client_descr )
{
    assert( m_network );
    LOG( LogType::INFO, MSG( "" ) );
    m_server_prx->initial_user_list( client_descr );
}

void ServerHandler::update_last_activity_time( const std::string& username, const std::string& new_last_activity )
{
    m_user_storage->update_last_activity_time( username, new_last_activity );
}

void ServerHandler::send_user_status_list( size_t client_descriptor, const std::string& encoded_status )
{
    emit signal_send_info( client_descriptor, encoded_status );
}

void ServerHandler::send_broadcast_user_status_list( const std::string& encoded_status )
{
    emit signal_send_broadcast_info( encoded_status );
}

void ServerHandler::send_new_user_status( size_t client_descriptor, const std::string& encoded_status )
{
    emit signal_send_new_user_status( client_descriptor, encoded_status );
}
