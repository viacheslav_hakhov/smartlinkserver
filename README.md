
In file CommonGraph.cmake change 21 line:

        include(CommonTarget) 
        =>
        include(${CMAKE_CURRENT_LIST_DIR}/CommonTarget.cmake)


PROTOBUF INSTALLATION

$ sudo apt update<br/>
$ sudo apt install protobuf-compiler

sudo apt-get update
sudo apt-get install liblog4cplus-dev

Look for "FindLog4cplus.cmake" in <path_to_Qt_application/Tools/CMake/share/cmake-<version>/Modules<br/>
If there is no such file - download it from github and add it to cmake modules:<br />
	$ cd <path_to_Qt_application>/Tools/CMake/share/cmake-<version>/Modules  
	$ sudo cp ~/Downloads/FindLog4cplus.cmake .  
        $ cd /usr/share/cmake-<version>/Modules      
        $ sudo cp ~/Downloads/FindLog4cplus.cmake .  
	$ sudo chmod 666 FindLog4cplus.cmake   

To install RapidJSON - download and unzip github-repository into Download folder :  https://github.com/Tencent/rapidjson/  
	$ cd /usr/include  
	$ sudo cp -r ~/Downloads/rapidjson-master/include/rapidjson/ .  
    	$ cd ~/Downloads/rapidjson-master/   
    	$ git init  
    	$ git submodule update --init  
     	$ mkdir build  
    	$ cd build/  
    	$ cmake ..  
    	$ make  
     	$ sudo make install  
